package com.iteco.alexline.structure_pattern;

import com.iteco.alexline.structure_pattern.entity.User;
import com.iteco.alexline.structure_pattern.entity.chat.Chat;
import com.iteco.alexline.structure_pattern.entity.chat.EncryptedChat;
import com.iteco.alexline.structure_pattern.entity.chat.IChat;

public class Main {

    public static void main(String[] args) {
        IChat chat = new EncryptedChat(new Chat());
        User alex = new User("Alex");
        User mike = new User("Mike");

        chat.receiveMessage(alex.sendMessage(mike, "Hi, Mike!"));
        chat.receiveMessage(alex.sendMessage(mike, "How are you?"));

        System.out.println("- - - - - - - - - - - - - -");

        chat.getMessagesForUser(mike.getName()).forEach(System.out::println);

        chat.receiveMessage(mike.sendMessage(alex, "what's up, bro?"));

        System.out.println("- - - - - - - - - - - - - -");

        chat.getMessagesForUser(alex.getName()).forEach(System.out::println);
    }

}