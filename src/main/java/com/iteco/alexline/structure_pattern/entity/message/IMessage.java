package com.iteco.alexline.structure_pattern.entity.message;

public interface IMessage {

    String getSender();

    String getReceiver();

    String getMessage();

}