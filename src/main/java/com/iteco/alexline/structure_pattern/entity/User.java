package com.iteco.alexline.structure_pattern.entity;

import com.iteco.alexline.structure_pattern.entity.chat.IChat;
import com.iteco.alexline.structure_pattern.entity.message.IMessage;
import com.iteco.alexline.structure_pattern.entity.message.Message;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public Message sendMessage(@NotNull final User receiver, @NotNull final String message) {
        return new Message(this.name, receiver.getName(), message);
    }

    public List<IMessage> getMessages(@NotNull final IChat IChat) {
        return IChat.getMessagesForUser(this.name);
    }

}