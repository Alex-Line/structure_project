package com.iteco.alexline.structure_pattern.entity.chat;

import com.iteco.alexline.structure_pattern.entity.message.IMessage;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public class Chat implements IChat {

    private final Map<IMessage, String> chat;

    public Chat() {
        chat = new LinkedHashMap<>();
    }

    public Chat(Map<IMessage, String> chat) {
        this.chat = chat;
    }

    @Override
    public void receiveMessage(@NotNull final IMessage message) {
        chat.putIfAbsent(message, message.getReceiver());
    }

    @Override
    public List<IMessage> getMessagesForUser(@NotNull final String user) {
        return chat.entrySet().stream()
                .filter(entry -> user.equalsIgnoreCase(entry.getValue()))
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());
    }

}