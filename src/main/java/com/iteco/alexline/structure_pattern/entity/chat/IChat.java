package com.iteco.alexline.structure_pattern.entity.chat;

import com.iteco.alexline.structure_pattern.entity.message.IMessage;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IChat {

    void receiveMessage(@NotNull final IMessage message);

    List<IMessage> getMessagesForUser(@NotNull final String user);

}