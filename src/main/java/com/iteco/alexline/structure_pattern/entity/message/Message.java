package com.iteco.alexline.structure_pattern.entity.message;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public class Message implements IMessage {

    private final String sender;

    private final String receiver;

    private String message;

    public Message() {
        this.sender = null;
        this.receiver = null;
    }

    public Message(@NotNull final String sender, @NotNull final String receiver, @NotNull final String message) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("Message: %s \n" +
                "from: %s \n" +
                "to: %s \n", message, sender, receiver);
    }
}