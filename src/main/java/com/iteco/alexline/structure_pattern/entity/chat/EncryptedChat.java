package com.iteco.alexline.structure_pattern.entity.chat;

import com.iteco.alexline.structure_pattern.UserMockDataLayer;
import com.iteco.alexline.structure_pattern.entity.message.IMessage;
import com.iteco.alexline.structure_pattern.entity.message.Message;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

public class EncryptedChat implements IChat {

    private final Chat chat;

    private final UserMockDataLayer userMockDataLayer;

    private Cipher cipher;

    {
        try {
            this.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public EncryptedChat() {
        this.chat = new Chat();
        this.userMockDataLayer = new UserMockDataLayer();

    }

    public EncryptedChat(@NotNull final Chat chat) {
        this.chat = chat;
        this.userMockDataLayer = new UserMockDataLayer();
    }

    @Override
    public void receiveMessage(@NotNull final IMessage message) {
        if (message.getMessage() == null) return;
        @NotNull final String encryptedSender = userMockDataLayer.getEncryptedUser(message.getSender());
        @NotNull final String encryptedReceiver = userMockDataLayer.getEncryptedUser(message.getReceiver());
        @Nullable final String encryptedMessage = encryptedMessage(message.getMessage());
        chat.receiveMessage(new Message(encryptedSender, encryptedReceiver, encryptedMessage));
        System.out.println(String.format("Message: %s \n" +
                "from: %s \n" +
                "to: %s \n" +
                "was added to chat.", encryptedMessage, encryptedSender, encryptedReceiver));
    }

    @Override
    public List<IMessage> getMessagesForUser(@NotNull final String user) {
        @NotNull final String encryptedReceiver = userMockDataLayer.getEncryptedUser(user);
        @NotNull final List<IMessage> encryptedMessages = chat.getMessagesForUser(encryptedReceiver);
        @NotNull final List<IMessage> decryptedMessages = encryptedMessages.stream()
                .map(message -> {
                    @NotNull final String decryptedSender = userMockDataLayer.getDecryptedUser(message.getSender());
                    @NotNull final String decryptedReceiver = userMockDataLayer.getDecryptedUser(message.getReceiver());
                    @Nullable String decryptedMessage = decryptedMessage(message.getMessage());
                    if (decryptedMessage == null) decryptedMessage = "Message cannot be viewing";
                    return new Message(decryptedSender, decryptedReceiver, decryptedMessage);
                }).collect(Collectors.toList());
        return decryptedMessages;
    }

    @NotNull
    private String encryptedMessage(@NotNull final String message) {
        return "<encrypted>" + message + "<encrypted>";
    }

    @NotNull
    private String decryptedMessage(@NotNull final String message) {
        return message.replaceAll("<encrypted>", "");
    }

}