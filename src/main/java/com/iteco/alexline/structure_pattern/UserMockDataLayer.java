package com.iteco.alexline.structure_pattern;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@NoArgsConstructor
public class UserMockDataLayer {

    private static final Map<String, String> userDB = new HashMap<>();

    public String getEncryptedUser(@NotNull final String user) {
        @Nullable String encryptedUser = userDB.get(user);
        if (encryptedUser == null) encryptedUser = "user_" + new Random().nextInt();
        userDB.put(user, encryptedUser);
        return encryptedUser;
    }

    public String getDecryptedUser(@NotNull final String encryptedUser) {
        @Nullable String decryptedUser = userDB.entrySet().stream()
                .filter(entry -> encryptedUser.equalsIgnoreCase(entry.getValue()))
                .map(entry -> entry.getKey())
                .collect(Collectors.toList())
                .get(0);
        if (decryptedUser == null) return "User is not found";
        return decryptedUser;
    }

}